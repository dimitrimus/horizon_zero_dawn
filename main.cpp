#include <iostream>
#include "src/SharedPointer.h"


//class MyObject {
//    int num;
//  public:
//    MyObject(int num) {
//        this->num = num;
//        std::cout << "object " << num << " is created" << std::endl;
//    }
//    ~MyObject() {
//        std::cout << "object " << num << " is destroyed" << std::endl;
//    }
//};
//
//int main() {
//    std::cout << "1" << std::endl;
//    SharedPointer<MyObject> ptr1(new MyObject(1));
//    {
//        std::cout << "2" << std::endl;
//        SharedPointer<MyObject> ptr2(new MyObject(2));
//        std::cout << "4" << std::endl;
//        ptr2 = ptr1;
//        std::cout << "5" << std::endl;
//    }
//
//    std::cout << "6" << std::endl;
//
//    return 0;
//}

#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <mutex>

struct Base
{
    Base() { std::cout << "  Base::Base() "; }
    // Note: non-virtual destructor is OK here
    ~Base() { std::cout << "  Base::~Base() "; }
};

struct Derived: public Base
{
    Derived() { std::cout << "  Derived::Derived()\n"; }
    ~Derived() { std::cout << "  Derived::~Derived()\n"; }
};

void thr(SharedPointer<Base> p)
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
    SharedPointer<Base> lp = p; // thread-safe, even though the
    // shared use_count is incremented
    {
        static std::mutex io_mutex;
        std::lock_guard<std::mutex> lk(io_mutex);
        std::cout << lp.count() << ' ';
    }
}

int main()
{
    for (int i = 0; i < 100; ++i) {
        SharedPointer<Base> p(new Base);

        std::thread t1(thr, p), t2(thr, p), t3(thr, p), t4(thr, p), t5(thr, p);

        p.reset();

        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();

        std::cout << i << " Pointer p.get() = " << p.get()
                  << " has " << p.count()
                  << " reference after threads joined (should be equal 1)." << std::endl;
    }
}
