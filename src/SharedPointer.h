//
// Created by Dmitry Sychev on 25/04/2018.
//
// std::shared_ptr is a smart pointer that retains shared ownership of an object through a pointer.
// Several shared_ptr objects may own the same object.
// The object is destroyed and its memory deallocated when either of the following happens:
//   - the last remaining shared_ptr owning the object is destroyed;
//   - the last remaining shared_ptr owning the object is assigned another pointer via operator= or reset().
//

#pragma once

#include <mutex>

template <class T>
class SharedPointer {
public:
    SharedPointer(T* other);

    SharedPointer(const SharedPointer<T>& other);
    SharedPointer& operator=(const SharedPointer<T>& other);

    virtual ~SharedPointer();

    T* get();
    int count() const;
    void reset();

private:
    int* refCounter;
    T* object;
    std::mutex* counterMutex;

    void decrementRef();
    void setOther(const SharedPointer<T>& other);
};


template <class T>
SharedPointer<T>::SharedPointer(T* other)
    : refCounter(new int(1))
    , object(other)
    , counterMutex(new std::mutex)
{}

template<class T>
SharedPointer<T>::SharedPointer(const SharedPointer<T>& other) {
    setOther(other);
}

template <class T>
void SharedPointer<T>::decrementRef() {
    if (refCounter == 0 || object == 0) {
        return;
    }

    {
        std::lock_guard<std::mutex> guard(*counterMutex);
        --(*refCounter);
    }

    if (*refCounter == 0) {
        delete object;
        delete refCounter;
        delete counterMutex;
    }
    refCounter = 0;
    object = 0;
    counterMutex = 0;
}

template <class T>
SharedPointer<T>::~SharedPointer() {
    decrementRef();
}

template <class T>
SharedPointer<T>& SharedPointer<T>::operator=(const SharedPointer<T>& other) {
    if (object == other.object) {
        return *this;
    }
    if (object) {
        decrementRef();
    }

    setOther(other);

    return *this;
}

template<class T>
T* SharedPointer<T>::get() {
    return object;
}

template<class T>
int SharedPointer<T>::count() const {
    if (refCounter == 0) {
        return 0;
    }
    return *refCounter;
}

template<class T>
void SharedPointer<T>::setOther(const SharedPointer<T> &other) {
    object = other.object;
    refCounter = other.refCounter;
    counterMutex = other.counterMutex;
    {
        std::lock_guard<std::mutex> guard(*counterMutex);
        ++(*refCounter);
    }
}

template<class T>
void SharedPointer<T>::reset() {
    decrementRef();
}
